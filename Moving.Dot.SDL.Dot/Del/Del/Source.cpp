/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, and strings
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_net.h>
#include <SDL_ttf.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include "TcpListener.h"
#include <stdlib.h>

#include <iostream>
#include <vector>

#include <time.h>

using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
void Listener_MessageReceived(CTcpListener* listener, int client, string msg);

//Texture wrapper class
class LTexture
{
public:
	//Initializes variables
	LTexture();

	//Deallocates memory
	~LTexture();

	//Loads image at specified path
	bool loadFromFile(std::string path);

#ifdef _SDL_TTF_H
	//Creates image from font string
	bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
#endif

	//Deallocates texture
	void free();

	//Set color modulation
	void setColor(Uint8 red, Uint8 green, Uint8 blue);

	//Set blending
	void setBlendMode(SDL_BlendMode blending);

	//Set alpha modulation
	void setAlpha(Uint8 alpha);

	//Renders texture at given point
	void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Gets image dimensions
	int getWidth();
	int getHeight();

private:
	//The actual hardware texture
	SDL_Texture* mTexture;

	//Image dimensions
	int mWidth;
	int mHeight;
};
//The dot that will move around on the screen
class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 20;
	static const int DOT_HEIGHT = 20;

	//Maximum axis velocity of the dot
	static const int DOT_VEL = 10;

	//Initializes the variables
	Dot();

	//Takes key presses and adjusts the dot's velocity
	void handleEvent(SDL_Event& e);

	//Moves the dot
	void move();

	//Shows the dot on the screen
	void render(int id);

	//The X and Y offsets of the dot
	int mPosX, mPosY;

	//The velocity of the dot
	int mVelX, mVelY;

	
};

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Scene textures
LTexture gDotTexture;
LTexture gDotTextureRed;
LTexture gDotTextureGreen;

LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	//Deallocate
	free();
}

bool LTexture::loadFromFile(std::string path)
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{
			//Get image dimensions
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

//#ifdef _SDL_TTF_H
//bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
//{
//	//Get rid of preexisting texture
//	free();
//
//	//Render text surface
//	TTF_Font gFont;
//	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
//	if (textSurface != NULL)
//	{
//		//Create texture from surface pixels
//		mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
//		if (mTexture == NULL)
//		{
//			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
//		}
//		else
//		{
//			//Get image dimensions
//			mWidth = textSurface->w;
//			mHeight = textSurface->h;
//		}
//
//		//Get rid of old surface
//		SDL_FreeSurface(textSurface);
//	}
//	else
//	{
//		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
//	}
//
//
//	//Return success
//	return mTexture != NULL;
//}
//#endif

void LTexture::free()
{
	//Free texture if it exists
	if (mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	//Modulate texture rgb
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void LTexture::setBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(mTexture, blending);
}

void LTexture::setAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void LTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}


Dot::Dot()
{
	//Initialize the offsets
	mPosX = 0;
	mPosY = 0;
	
	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
}

void Dot::handleEvent(SDL_Event& e)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: mVelY -= DOT_VEL; break;
		case SDLK_DOWN: mVelY += DOT_VEL; break;
		case SDLK_LEFT: mVelX -= DOT_VEL; break;
		case SDLK_RIGHT: mVelX += DOT_VEL; break;
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: mVelY += DOT_VEL; break;
		case SDLK_DOWN: mVelY -= DOT_VEL; break;
		case SDLK_LEFT: mVelX += DOT_VEL; break;
		case SDLK_RIGHT: mVelX -= DOT_VEL; break;
		}
	}
}

void Dot::move()
{
	//Move the dot left or right
	mPosX += mVelX;

	//If the dot went too far to the left or right
	//if ((mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH))
	//{
	//	//Move back
	//	mPosX -= mVelX;
	//}

	//Move the dot up or down
	mPosY += mVelY;

	//If the dot went too far up or down
	//if ((mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT))
	//{
	//	//Move back
	//	mPosY -= mVelY;
	//}
}

bool Collision(Dot* dot, Dot* dot2)
{
	bool isCollising = false;
	if (dot->mPosX > dot2->mPosX && dot->mPosX < dot2->mPosX + dot2->DOT_WIDTH)
	{
		if (dot->mPosY > dot2->mPosY && dot->mPosY < dot2->mPosY + dot2->DOT_HEIGHT)
		{
			isCollising = true;
		}
	}
	return isCollising;
}

void Dot::render(int id)
{
	//Show the dot
	if(id == 0)
		gDotTexture.render(mPosX, mPosY);
	else if(id == 1)
		gDotTextureRed.render(mPosX, mPosY);
	else
		gDotTextureGreen.render(mPosX, mPosY);
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia(string name)
{
	//Loading success flag
	bool success = true;
	
	//Load dot texture
	if (!gDotTexture.loadFromFile("dotblue.bmp"))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}
	gDotTextureRed.loadFromFile("dotred.bmp");
	gDotTextureGreen.loadFromFile("dotgreen.bmp");

	return success;
}

void close()
{
	//Free loaded images
	gDotTexture.free();

	//Destroy window	
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

struct Move
{
	bool pressedLeft;
	bool pressedRight;
	bool fired;
};

class player
{
private:
	int xPos, yPos;
	int health;
};

struct msg
{
	int b;
};

class PacketStream 
{ 
private:       
	ostringstream outputStream;       
	istringstream inputStream; 
public:
	void readInt(int &);
	void writeInt(int);
	void toCharArray(char* );      
	void fromCharArray(char* );
};

class GameEntity
{
private:
	int positionX;
	int positionY;
	int currentHealth;
public:
	void receivePacketData(PacketStream & streamIn);
	void writePacketData(PacketStream & streamIn);
};

void GameEntity::writePacketData(PacketStream & streamIn)
{
	streamIn.writeInt(positionX);
	streamIn.writeInt(positionY);
	streamIn.writeInt(currentHealth);
}

void GameEntity::receivePacketData(PacketStream & streamIn)
{
	streamIn.readInt(positionX);
	streamIn.readInt(positionY);
	streamIn.readInt(currentHealth);
}


enum MESSAGETYPE {PLAYERJOINED, PLAYERLEFT, UPDATEOBJECT, ENDMESSAGE, OBJECTCREATED, OBJECTSTATEUPDATE, OBJECTDELETED};

void MakeScreen(Dot* shape, int minX, int maxX, int minY, int maxY)
{
	if (shape->mPosX > maxX)
		shape->mPosX = shape->mPosX - maxX;
	if (shape->mPosX < minX)
		shape->mPosX = shape->mPosX + maxX - minX;

	if (shape->mPosY > maxY)
		shape->mPosY = shape->mPosY - maxY;
	if (shape->mPosY < minY)
		shape->mPosY = shape->mPosY + maxY - minY;
}

void GameOver(Dot* dot, SDL_Renderer* renderer, TCPsocket* sock)
{
	//printf("Game Over");
	char gameOver[1400];
	sprintf_s(gameOver, "r");
	SDLNet_TCP_Send(*sock, gameOver, strlen(gameOver) + 1);
	/*
	TTF_Font* Sans = TTF_OpenFont("calibri.ttf", 24);

	SDL_Color White = { 255, 255, 255 };

	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(Sans, "put your text here", White); 

	SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);

	SDL_Rect Message_rect; 
	Message_rect.x = 0; 
	Message_rect.y = 0; 
	Message_rect.w = 100; 
	Message_rect.h = 100;

	SDL_RenderCopy(renderer, Message, NULL, &Message_rect);
	*/
}

void MakeServer()
{
	struct data 
	{
		TCPsocket socket;
		int id;
		Uint32 timeOut;
		data(TCPsocket sock, int i, Uint32 t) : socket(sock), id(i), timeOut(t) {};
	};

	init();
	SDLNet_Init();
	int curId = 0, playerNum = 0;
	IPaddress ip;
	SDLNet_ResolveHost(&ip, NULL, 1234);
	TCPsocket server = SDLNet_TCP_Open(&ip);
	SDLNet_SocketSet sockets = SDLNet_AllocSocketSet(100);
	char tmp[1400];
	std::vector<data> socketvector;
	SDL_Event event;

	bool running = true;
	bool paused = false;
	float time = 0;

	bool has3Connected = false;

	while (running)
	{
		
		clock_t beginTime = clock();
		while (SDL_PollEvent(&event))
		{
			if ((event.type == SDL_QUIT))
			{
				running = false;
			}
		}
		if (paused)
			continue;
		TCPsocket tempSock = SDLNet_TCP_Accept(server);
		if (tempSock)
		{
			if (playerNum < 100)
			{
				sprintf_s(tmp, "c%d\n", curId);
				printf("connention! %d\n" + curId);
				SDLNet_TCP_AddSocket(sockets, tempSock);
				socketvector.push_back(data(tempSock, SDL_GetTicks(), curId));
				playerNum++;
				curId++;
			}
			else
			{
				sprintf_s(tmp, "3 \n");
			}
			SDLNet_TCP_Send(tempSock, tmp, strlen(tmp));
		}

		has3Connected = socketvector.size() >= 3;
		char gameOver[1400];
		while (SDLNet_CheckSockets(sockets, 0) > 0)
		{
			for (int i = 0 ; i < socketvector.size(); i++)
			{
				if (SDLNet_SocketReady(socketvector[i].socket))
				{
					socketvector[i].timeOut = SDL_GetTicks();
					memset(tmp, 0, size(tmp));
					SDLNet_TCP_Recv(socketvector[i].socket, tmp, 1400);
					if (tmp[0] == 'r')
					{
						printf(tmp);
					}

					for (int k = 0; k < socketvector.size(); k++)
					{
						if (k == i)
							continue;
						SDLNet_TCP_Send(socketvector[k].socket, tmp, strlen(tmp) + 1);
					}

					if (tmp != NULL)
					{
						if (tmp[0] == 'r')
						{
							memset(gameOver, 0, size(gameOver));
							sprintf_s(gameOver, "r%d\n", time);
							for (int k = 0; k < socketvector.size(); k++)
							{
								SDLNet_TCP_Send(socketvector[k].socket, gameOver, strlen(gameOver) + 1);
								//paused = true;
								cout << k;
							}
						}
					}

					//int num = tmp[0] - '0';
					//if (num == 1)
					//{
					//	for (int k = 0; k < socketvector.size(); k++)
					//	{
					//		if (k == i)
					//			continue;
					//		SDLNet_TCP_Send(socketvector[k].socket, tmp, strlen(tmp));
					//	}
					//}
					//else if (num == 3)
					//{
					//	// disconnect
					//}
				}
			}
		}

		if (has3Connected)
		{
			clock_t end = clock();
			double elapsed_secs = double(end - beginTime) / CLOCKS_PER_SEC;
			time += elapsed_secs;
			//cout << time;
		}
		//printf(time);
	}

	SDLNet_Quit();
	SDL_Quit();
}

void MakeClient()
{
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		if (!loadMedia("dot.bmp"))
		{
			printf("Failed to load media!\n");
		}
		else
		{
			string ipAdres = "149.153.106.145";
			IPaddress ip;
			TCPsocket sock;
			char message[1024];
			int len;
			Uint16 port = 1234;
			int id = 0;

			bool paused = false;

			if (SDL_Init(0) == -1)
			{
				printf("SDL_Init: %s\n", SDL_GetError());
				exit(1);
			}

			if (SDLNet_Init() == -1)
			{
				printf("SDLNet_Init: %s\n", SDLNet_GetError());
				exit(2);
			}

			if (SDLNet_ResolveHost(&ip, ipAdres.c_str(), port) == -1)
			{
				printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
				exit(3);
			}

			sock = SDLNet_TCP_Open(&ip);
			if (!sock)
			{
				printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
				exit(4);
			}

			//TTF_Init();
			bool quit = false;
			int po;
			SDL_Event e;

			Dot dot;
			Dot dot2;
			Dot dot3;
			GameEntity gameEntity;

			while (!quit)
			{
				while (SDL_PollEvent(&e) != 0)
				{
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					dot.handleEvent(e);
				}
				char inputsGot[1400];
				memset(inputsGot, 0, sizeof(inputsGot));
				po = SDLNet_TCP_Recv(sock, inputsGot, 1400);
				if (paused)
				{
					continue;
				}
				dot.move();

				MakeScreen(&dot, 0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);

				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear(gRenderer);
				// Collisions
				if (id == 0)
				{
					if (Collision(&dot, &dot2))
					{
						GameOver(&dot2, gRenderer, &sock);
					}
					if (Collision(&dot, &dot3))
					{
						GameOver(&dot3, gRenderer, &sock);
					}
				}
				char tmp[1400];
				sprintf_s(tmp, "%d>m%d>%d\n", id, dot.mPosX, dot.mPosY);
				int result = SDLNet_TCP_Send(sock, tmp, strlen(tmp) + 1);

				

				//if (id == 1)
				//{
				//	if (Collision(&dot, &dot2))
				//	{
				//		GameOver(&dot2, gRenderer, &sock);
				//		paused = true;
				//	}

				//	if (Collision(&dot3, &dot2))
				//	{
				//		GameOver(&dot2, gRenderer, &sock);
				//		paused = true;
				//	}
				//}

				//if (id == 2)
				//{
				//	if (Collision(&dot, &dot3))
				//	{
				//		GameOver(&dot3, gRenderer, &sock);
				//		paused = true;
				//	}

				//	if (Collision(&dot2, &dot3))
				//	{
				//		GameOver(&dot3, gRenderer, &sock);
				//		paused = true;
				//	}
				//}

				
				if (po > 0)
				{
					/*if (inputsGot[0] == 'e')
					{
						GameOver(&dot, gRenderer);
						paused = true;
					}*/
					if (inputsGot[0] == 'r')
					{
						double time = inputsGot[1] - '0';
						cout << "Time alive:";
						cout << time;
						paused = true;
					}
					int idToChange = inputsGot[0] - '0';
					// Movement
					if (inputsGot[2] == 'm')
					{
						printf(inputsGot);

						int num = inputsGot[3] - '0';
						int j = 4;
						while (inputsGot[j] >= '0' && inputsGot[j] <= '9' && inputsGot[j] != '>')
						{
							num *= 10;
							num += inputsGot[j] - '0';
							j++;
						}

						j++;

						if (idToChange == 1)
							dot2.mPosX = num;
						else if (idToChange == 2)
							dot3.mPosX = num;
						else if (idToChange == 0 && (id == 1 || id == 2))
						{
							if (id == 1)
							{
								dot2.mPosX = num;
							}
							else
							{
								dot3.mPosX = num;
							}
						}

						num = inputsGot[j] - '0';
						j++;
						while (inputsGot[j] >= '0' && inputsGot[j] <= '9')
						{
							num *= 10;
							num += inputsGot[j] - '0';
							j++;
						}

						if (idToChange == 1)
							dot2.mPosY = num;
						else if (idToChange == 2)
							dot3.mPosY = num;
						else if (idToChange == 0 && (id == 1 || id == 2))
						{
							if (id == 1)
							{
								dot2.mPosY = num;
							}
							else
							{
								dot3.mPosY = num;
							}
						}
					} 
					// Making and ID getting
					if (inputsGot[0] == 'c')
					{
						id = inputsGot[1] - '0';
						if (id == 1)
						{
							dot.mPosX = 50;
						}
						else if (id == 2)
						{
							dot.mPosX = 150;
						}
					} 
					// Game over
					
				}
				dot.render(id); // 0 , 1 , 2

				// Render dots
				if (id == 0)
				{
					dot2.render(1);
					dot3.render(2);
				}
				else if (id == 1)
				{
					dot2.render(0);
					dot3.render(2);
				}
				else if (id == 2)
				{
					dot2.render(1);
					dot3.render(0);
				}

				

				SDL_RenderPresent(gRenderer);
			}
		}
	}
}

int main(int argc, char* args[])
{
	std::cout << "press s for server, c for client \n";
	char c = getchar();


	if (c == 's')
		MakeServer();
	else
		MakeClient();

	std::cout << "Close";
	//Free resources and close SDL
	close();
	char d = getchar();
	return 0;
}



void PacketStream::writeInt(int dataIn)
{
	outputStream << dataIn << "";
}

void PacketStream::readInt(int& dataOut)
{

	inputStream >> dataOut;
}

void PacketStream::toCharArray(char* arrayIn) 
{ 
	string s = outputStream.str();    	      
	memcpy(arrayIn, s.c_str(), s.length());      
	outputStream.str(""); 
}

void PacketStream::fromCharArray(char* arrayIn) 
{
	inputStream.str("");  
	inputStream.str(arrayIn);
}

void Listener_MessageReceived(CTcpListener* listener, int client, string msg)
{
	if (msg != "")
	{
		listener->Send(client, msg);
	}
}